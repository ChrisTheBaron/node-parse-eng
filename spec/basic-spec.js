'use strict';

require('./helper.js');

describe('Normal functionality', function () {

	it('should be a callable function', function () {

		assert.isFunction(parseEng);

	})

	it('should return a number if it\'s just a number', function () {

		var values = [
			["123", 123],
			["12.3", 12.3],
			["0", 0],
			["0000", 0],
			["00123123", 123123],
			["-123", -123],
			["5e3", 5000],
			["-5e3", -5000],
			["5e-3", 0.005],
			["-5e-3", -0.005]
		];

		values.forEach(function (value) {

			var result = parseEng(value[0]);

			expect(result).to.equal(value[1]);

		});

	});

	it('should parse the number if it\'s a standard notation', function () {

		var values = [
			["12k", 12000],
			["1k2", 1200]
		];

		values.forEach(function (value) {

			var result = parseEng(value[0]);

			expect(result).to.equal(value[1]);

		});

	});

});

describe('Edge cases', function () {

	it('should throw an Error if a blank string is passed in and strict mode is on', function () {

		expect(parseEng.bind(parseEng, '')).to.throw("Cannot parse blank string");

	});

	it('should throw an Error if the string doesn\'t match the expected format', function () {

		var invalids = [
			"123P",
			"---123",
			"123dickbutt"
		];

		invalids.forEach(function (value) {
			expect(parseEng.bind(parseEng, value)).to.throw("Invalid syntax: " + value);
		})

	});

});
