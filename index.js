var debug = require('debug')('parse-eng');

var multipliers = {
	"p": -12,
	"n": -9,
	"u": -6,
	"m": -3,
	"k": 3,
	"K": 3,// Just in case. People aren't perfect you know
	"M": 6,
	"G": 9
};

var multiRegex = Object.keys(multipliers).join('|');

debug("multiRegex: " + multiRegex);

var regexPtn = new RegExp(/^(\-?\d+(\.\d+)?)/.source + "(" + multiRegex + ")?" + /(\d+)?$/.source);

module.exports = function (string, options) {

	options = options || {};

	if (!options.hasOwnProperty("strict")) options.strict = true;

	debug("Called with:", string, options);

	if (string === '') {
		if (options.strict === true) {
			throw new Error("Cannot parse blank string");
		} else {
			return null;
		}
	}

	if (!isNaN(string)) {
		return parseFloat(string);
	}

	var matches = regexPtn.exec(string);

	debug(matches);

	/*
	 * Here's where it's about to get complicated
	 */

	/*
	 * If we don't have any matches then it's invalid syntax
	 * so we better raise an Error.
	 */
	if (!matches) {
		if (options.strict === true) {
			throw new Error("Invalid syntax: " + string);
		} else {
			return null;
		}
	}

	var multiplier = Math.pow(10, multipliers[matches[3]]);

	debug("Going to multiply by: " + multiplier);

	/*
	 * We don't want a decimal point with numbers after it
	 * AND numbers after the multiplier. That doesn't make sense.
	 */
	if (matches[2] && matches[4]) {
		if (options.strict === true) {
			throw new Error("Invalid syntax: " + string);
		} else {
			return null;
		}
	}

	var number;

	if (matches[4]) {
		number = parseFloat(matches[1] + '.' + matches[4]);
	} else {
		number = parseFloat(matches[1]);
	}

	debug("Number without multiplying is: " + number);

	return number * multiplier;

};
